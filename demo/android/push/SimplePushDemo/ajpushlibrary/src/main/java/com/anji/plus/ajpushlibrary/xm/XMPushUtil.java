package com.anji.plus.ajpushlibrary.xm;

import android.content.Context;

import com.xiaomi.mipush.sdk.MiPushClient;

/**
 * Created by wuyan on 2020/12/7.
 */
public class XMPushUtil {
    private static final String TAG = "XMPushUtil";
    private Context context;

    public XMPushUtil(Context context) {
        this.context = context;
    }

    //设置别名
    public void setAlias(String alias) {
        MiPushClient.setAlias(context, alias, null);
    }

    //撤销别名
    public void unsetAlias(String alias) {
        MiPushClient.unsetAlias(context, alias, null);
    }

    //设置帐号
    public void setAccount(String account) {
        MiPushClient.setUserAccount(context, account, null);
    }

    //撤销帐号
    public void unsetAccount(String account) {
        MiPushClient.unsetUserAccount(context, account, null);
    }

    //设置标签，不同手机上的同一个app可以订阅同一个主题
    public void subscribeTopic(String topic) {
        MiPushClient.subscribe(context, topic, null);
    }

    //撤销标签
    public void unsubscribeTopic(String topic) {
        MiPushClient.unsubscribe(context, topic, null);
    }

    //暂停推送
    public void pausePush() {
        MiPushClient.pausePush(context, null);
    }

    //恢复推送
    public void resumePush() {
        MiPushClient.resumePush(context, null);
    }

    public String getRegId(Context context) {
        return MiPushClient.getRegId(context);
    }

}


