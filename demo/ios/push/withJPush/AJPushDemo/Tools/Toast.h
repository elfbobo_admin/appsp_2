//
//  Toast.h
//  AJPushDemo
//
//  Created by kean_qi on 2021/3/18.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

enum AJTimeType
{
    AJLongTime,
    AJShortTime
};

@interface Toast : UIView{
    UILabel* _label;
    NSString * _text;
    CGFloat _time;
}
@property (copy, nonatomic) NSString *theNewText;

+(Toast *)makeText:(NSString *)text;
+(Toast *)makeUpText:(NSString *)text;
+(void)aj_makeUpText:(NSString *)text showWithType:(enum AJTimeType)type;
-(void)showWithType:(enum AJTimeType)type;

@end
NS_ASSUME_NONNULL_END
