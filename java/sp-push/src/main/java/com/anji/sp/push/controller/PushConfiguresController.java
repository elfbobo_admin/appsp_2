package com.anji.sp.push.controller;

import com.anji.sp.aspect.PreSpAuthorize;
import com.anji.sp.model.RequestModel;
import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.vo.PushConfiguresVO;
import com.anji.sp.push.service.PushConfiguresService;
import com.anji.sp.util.BeanUtils;
import com.anji.sp.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * <p>
 * 推送配置项 前端控制器
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Api(tags = "推送配置")
@RestController
@RequestMapping("/pushConfigures")
public class PushConfiguresController {

    @Autowired
    private PushConfiguresService pushConfiguresService;

    @PostMapping("/save")
    @ApiOperation(value = "保存/id有更新没有创建", httpMethod = "POST")
    @PreSpAuthorize("system:user:push")
    public ResponseModel save(@RequestBody PushConfiguresVO requestModel) {
        if (StringUtils.isBlank(requestModel.getAppKey())){
            return ResponseModel.errorMsg("appKey不能为空");
        }
        PushConfiguresVO v = new PushConfiguresVO();
        BeanUtils.copyProperties(requestModel, v);
        ResponseModel rep = pushConfiguresService.select(v);
        //如果 配置存在 更新
        if (rep.isSuccess()){
            PushConfiguresVO r = (PushConfiguresVO) rep.getRepData();
            if (Objects.nonNull(r)){
                requestModel.setId(r.getId());
                return pushConfiguresService.updateById(requestModel);
            }
        }
        return pushConfiguresService.create(requestModel);
    }

    @PostMapping("/select")
    @ApiOperation(value = "根据appKey或appId查询一条记录", httpMethod = "POST")
    @PreSpAuthorize("system:user:push")
    public ResponseModel select(@RequestBody PushConfiguresVO requestModel) {
        return pushConfiguresService.select(requestModel);
    }

    @PostMapping("/queryByAppKey")
    @PreSpAuthorize("system:user:push")
    public ResponseModel queryByAppKey(@RequestBody PushConfiguresVO requestModel) {
        return pushConfiguresService.queryByAppKey(requestModel);
    }

    @PostMapping("/queryByPage")
    @PreSpAuthorize("system:user:push")
    public ResponseModel queryByPage(@RequestBody PushConfiguresVO requestModel) {
        return pushConfiguresService.queryByPage(requestModel);
    }
}

