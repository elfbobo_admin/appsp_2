package com.anji.sp.push.controller;


import com.anji.sp.aspect.PreSpAuthorize;
import com.anji.sp.model.RequestModel;
import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.vo.PushHistoryVO;
import com.anji.sp.push.service.PushHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 推送历史表 前端控制器
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Api(tags = "推送历史")
@RestController
@RequestMapping("/pushHistory")
public class PushHistoryController {

    @Autowired
    private PushHistoryService pushHistoryService;

    @PostMapping("/create")
    @PreSpAuthorize("system:user:push")
    public ResponseModel create(@RequestBody PushHistoryVO requestModel) {
        return pushHistoryService.create(requestModel);
    }

    @PostMapping("/updateById")
    @PreSpAuthorize("system:user:push")
    public ResponseModel updateById(@RequestBody PushHistoryVO requestModel) {
        return pushHistoryService.updateById(requestModel);
    }

    @PostMapping("/deleteById")
    @PreSpAuthorize("system:user:push")
    public ResponseModel deleteById(@RequestBody PushHistoryVO requestModel) {
        return pushHistoryService.deleteById(requestModel);
    }

    @PostMapping("/queryById")
    @PreSpAuthorize("system:user:push")
    public ResponseModel queryById(@RequestBody PushHistoryVO requestModel) {
        return pushHistoryService.queryById(requestModel);
    }

    @ApiOperation(value = "分页查询推送历史", httpMethod = "POST")
    @PostMapping("/queryByPage")
    @PreSpAuthorize("system:user:push")
    public ResponseModel queryByPage(@RequestBody PushHistoryVO requestModel) {
        return pushHistoryService.queryByPage(requestModel);
    }


    @ApiOperation(value = "根据msgId查询消息历史", httpMethod = "POST")
    @PostMapping("/queryByMsgId")
    @PreSpAuthorize("system:user:push")
    public ResponseModel queryByMsgId(@RequestBody PushHistoryVO requestModel) {
        return pushHistoryService.queryByMsgId(requestModel, 0);
    }
}

