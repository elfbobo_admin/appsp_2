package com.anji.sp.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 应用
 *
 * @author kean 2020-06-27
 */
@Data
public class SpAppShareVO {

    @ApiModelProperty("应用名称")
    private String name;
    @ApiModelProperty("logo Url")
    private String logoUrl;
    @ApiModelProperty("推广版本/环境信息")
    private String promoteVersion;
    @ApiModelProperty("推广应用名")
    private String promoteName;
    @ApiModelProperty("推广语")
    private String promoteDesc;
    @ApiModelProperty("推广介绍")
    private String promoteIntroduction;

    @ApiModelProperty("iOS下载地址")
    private String iOSUrl;
    @ApiModelProperty("iOS更新日志")
    private String iOSUpdateLog;
    @ApiModelProperty("android下载地址")
    private String androidUrl;
    @ApiModelProperty("android更新日志")
    private String androidUpdateLog;

}