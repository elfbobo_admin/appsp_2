import 'dart:async';

import 'package:flutter/services.dart';

typedef onMessageArrivedCallback = void Function(String result);
typedef onMessageClickCallback = void Function(String result);

///This is a plugin for Jpush flutter,gtui plugin will be completed later
class AjFlutterAppspPush {
  onMessageArrivedCallback _onMessageArrivedCallback;
  onMessageClickCallback _onMessageClickCallback;

  static const MethodChannel _channel =
      const MethodChannel('aj_flutter_appsp_push');

  AjFlutterAppspPush() {
    print("AjFlutterAppspPush constructor");
    _channel.setMethodCallHandler(_handlerMethodCallback);
  }

  ///消息回调，可接收多个消息
  void getMessageArrivedCallback(onMessageArrivedCallback callback) {
    if (callback == null) {
      return;
    }
    this._onMessageArrivedCallback = callback;
  }

  ///消息点击回调，可接收多个消息
  void getMessageClickCallback(onMessageClickCallback callback) {
    if (callback == null) {
      return;
    }
    this._onMessageClickCallback = callback;
  }

  /// native - flutter
  Future<dynamic> _handlerMethodCallback(MethodCall call) async {
    print("AjFlutterAppspPush call.method ${call.method}");
    switch (call.method) {
      case "onMessageArrived":
        print("AjFlutterAppspPush call.arguments ${call.arguments}");
        String result = call.arguments;
        print("AjFlutterAppspPush result ${result}");
        print("AjFlutterAppspPush onMessageArrived ");
        if (_onMessageArrivedCallback != null) {
          _onMessageArrivedCallback(result);
        }
        break;
      case "onMessageClicked":
        print("AjFlutterAppspPush call.arguments ${call.arguments}");
        String result = call.arguments;
        print("AjFlutterAppspPush result ${result}");
        print("AjFlutterAppspPush onMessageClicked ");
        if (_onMessageClickCallback != null) {
          _onMessageClickCallback(result);
        }
        break;
      default:
        break;
    }
  }

  ///获取设备ID
  static Future<String> getDeviceId() async {
    String result = await _channel.invokeMethod("getDeviceId", {});
    return result;
  }

}
